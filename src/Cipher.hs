module Cipher
  ( generateKeyFiles
  , encryptText
  , decryptAndWriteToFile
  , readKeyFile
  ) where

import Block (Block(..), BlockSize(..), blocksToString, stringToBlocks)
import Control.Monad.Loops (iterateUntil)
import Data.List (intercalate)
import Data.List.Split (splitOn)
import Math.NumberTheory.Euclidean as E (gcd)
import Math.NumberTheory.Powers.Modular (powMod)
import Math.NumberTheory.Primes.Testing (isPrime)
import System.Random

keySize = 1024

blockSize = BlockSize 169

encryptBlocks key message =
  let (n, e) = key
      blocks = stringToBlocks blockSize message
  in (\(Block b) -> powMod b e n) <$> blocks

decryptMessage key messageLength encryptedBlocks =
  let (n, d) = key
      blocks = (\(Block b) -> Block (powMod b d n)) <$> encryptedBlocks
  in blocksToString blockSize messageLength blocks

generateLargePrime :: IO Integer
generateLargePrime = iterateUntil isPrime generateRandom

generateRandom = do
  let minBound = 2 ^ (keySize - 1)
  let maxBound = 2 ^ (keySize)
  n <- randomRIO (minBound, maxBound)
  return n

generateLargePrimeTuple = do
  p <- generateLargePrime
  q <- generateLargePrime
  return (p, q)

findModInverse a m
  | E.gcd a m /= 1 = Nothing
  | otherwise =
    let (u1, u2, u3) = (1, 0, a)
        (v1, v2, v3) = (0, 1, m)
    in go (u1, u2, u3) (v1, v2, v3)
  where
    go (u1, u2, u3) (v1, v2, 0) = Just $ u1 `mod` m
    go (u1, u2, u3) (v1, v2, v3) =
      let q = u3 `div` v3
      in go (v1, v2, v3) (u1 - q * v1, u2 - q * v2, u3 - q * v3)

generateKeyFiles path = do
  (p, q) <- iterateUntil (\(p, q) -> p /= q) generateLargePrimeTuple
  let n = p * q
  e <- iterateUntil (\e -> (E.gcd e ((p - 1) * (q - 1))) == 1) generateRandom
  let mD = findModInverse e ((p - 1) * (q - 1))
  case mD of
    (Just d) -> do
      let publicKey = (n, e)
      let privateKey = (n, d)
      writeFile (path <> "/publicKey.txt") $
        (show keySize) <> "\n" <> (show $ fst publicKey) <> "\n" <>
        (show $ snd publicKey)
      writeFile (path <> "/privateKey.txt") $
        (show keySize) <> "\n" <> (show $ fst privateKey) <> "\n" <>
        (show $ snd privateKey)
      putStrLn $
        "generated keys at " <> path <> "/publicKey.txt and " <> path <>
        "/privateKey.txt"
    Nothing -> error "Could not find modular inverse."

readKeyFile :: FilePath -> IO (Integer, Integer)
readKeyFile filePath = do
  c <- readFile filePath
  let [size, x, y] = lines c
  return (read x, read y)

encryptText key plainText =
  let encryptedBlocks = encryptBlocks key plainText
      encryptedMessage = intercalate "," (map show encryptedBlocks)
      (BlockSize bS) = blockSize
  in (show $ length plainText) <> "_" <> (show bS) <> "_" <> encryptedMessage
  --writeFile ("encrypted.txt") encryptedContent

decryptAndWriteToFile key encryptedText =
  let [messageLength, blockSize, encryptedMessage] = splitOn "_" encryptedText
      encryptedBlocks =
        (map (Block . read) $ splitOn "," encryptedMessage) :: [Block]
  in decryptMessage key (read messageLength) encryptedBlocks
