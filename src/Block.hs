{-# LANGUAGE DeriveGeneric #-}

module Block
  ( Block(..)
  , BlockSize(..)
  , blocksToString
  , stringToBlocks
  ) where

import Data.List.Split (chunksOf)
import qualified Data.Map as Map
import Data.Validity (Validity, check, validate)
import GHC.Generics (Generic)

newtype Block =
  Block Integer
  deriving (Show, Ord, Eq, Generic)

instance Semigroup Block where
  (<>) (Block x) (Block y) = Block (x + y)

instance Monoid Block where
  mempty = Block 0
  mappend = (<>)

newtype BlockSize =
  BlockSize Integer
  deriving (Show, Ord, Eq, Generic)

-- symbols = ['A' .. 'Z'] <> ['a' .. 'z'] <> (map toEnum [48 .. 57]) <> " !?."
instance Validity Block where
  validate (Block b) = check (b > 0) "Must be greater than 0"

validChars =
  (['A' .. 'Z'] <> ['a' .. 'z'] <> (map toEnum [49 .. 57]) <> ['0'] <> " !?.\n")

validCharsWithIndex = Map.fromList $ zip validChars [0,1 ..]

indicesWithValidChars = Map.fromList $ zip [0,1 ..] validChars

charToBlock p c =
  let index = Map.lookup c validCharsWithIndex
      numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer
  in case index of
       (Just i) -> Block $ i * (numValidChars ^ p)
       _ -> error $ "Unsupported Symbol: " <> show c

chunkToBlock c =
  fst $ foldl (\(b, p) c -> (b <> (charToBlock p c), p + 1)) (Block 0, 0) c

stringToBlocks (BlockSize bS) s =
  map chunkToBlock (chunksOf (fromIntegral bS) s)

blocksToString (BlockSize bS) messageLength blocks =
  let remainderChars = (fromIntegral $ length blocks) * bS - messageLength
      lastBlockSize = bS - remainderChars
  in (concat . map reverse)
       ((blockToChunk (bS - 1) <$> init blocks) <>
        [(blockToChunk (lastBlockSize - 1) (last blocks))])

blockToChunk :: Integer -> Block -> String
blockToChunk bS (Block b) = go b bS
  where
    numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer
    go _ (-1) = ""
    go b bS =
      let (c, nextB) = blockToChar b bS
      in c : (go nextB (bS - 1))

blockToChar b p =
  let numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer
      (index, nextB) = b `divMod` (numValidChars ^ p)
      char = Map.lookup index indicesWithValidChars
  in case char of
       (Just c) -> (c, nextB)
       _ -> error $ "Out of bounds index " <> (show index)
