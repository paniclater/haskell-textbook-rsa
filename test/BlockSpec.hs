{-# LANGUAGE TypeApplications #-}

module BlockSpec where

import Block
import Test.Hspec
import Test.Validity

spec :: Spec
spec = do
  describe "Block" $ do
    it "should encode Howdy correctly" $ do
      stringToBlocks (BlockSize 169) "Howdy" `shouldBe` [Block 957285919]
  describe "stringToBlocks" $ do
    it "should encode 42! correctly" $ do
      stringToBlocks (BlockSize 169) "42!" `shouldBe` [Block 277981]
    it "should encode a sentence correctly" $ do
      stringToBlocks (BlockSize 169) "I named my cat Zophie." `shouldBe`
        [Block 10627106169278065987481042235655809080528]
    it "should encode a longer string correctly" $ do
      stringToBlocks (BlockSize 169) (s0 <> s1 <> s2) `shouldBe` [b0, b1, b2]
  describe "blocksToString" $ do
    it "should decode Howdy correctly" $ do
      blocksToString
        (BlockSize 169)
        (fromIntegral $ length "Howdy")
        [Block 957285919] `shouldBe`
        "Howdy"
    it "should decode 42! correctly" $ do
      blocksToString
        (BlockSize 169)
        (fromIntegral $ length "42!")
        [Block 277981] `shouldBe`
        "42!"
    it "should decode a longer string correctly" $ do
      blocksToString
        (BlockSize 169)
        (fromIntegral $ length (s0 <> s1 <> s2))
        [b0, b1, b2] `shouldBe`
        (s0 <> s1 <> s2)

s0 =
  "Alan Mathison Turing was a British cryptanalyst and computer scientist. He was highly influential in the development of computer science and provided a formalisation of "

s1 =
  "the concepts of algorithm and computation with the Turing machine. Turing is widely considered to be the father of computer science and artificial intelligence. During W"

s2 =
  "orld War II he worked for the Government Code and Cypher School at Bletchley Park."

b0 =
  Block
    30138103381200276581206111663322701590471547608326152595431391575797140707837485089852659286061395648657712401264848061468979996871106525448961558640277994456848107158423162065952633246425985956987627719631460939256595688769305982915401292341459466451137309352608735432166613773623460986403811099485392482698

b1 =
  Block
    11068907809221474552159350801956343731326801027081927136514840854754026777527919580758722720267087026340702811097095557610085841376819190225258032442691476944762174257333902148064107269871669093655004577014280290424452471175143504911739898604483879159731507893719486011257479801658756445279245156715863348631

b2 =
  Block
    158367975496160191442895244721758369787583763597486412804750943905655902273209591807729054194485980905328691576422832688749509527709935741799076979034
