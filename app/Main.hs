module Main where

import Cipher
import System.Environment (getArgs)
import System.Exit (die)

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["encrypt", keyPath, plainTextPath] -> do
      plainText <- readFile plainTextPath
      key <- readKeyFile keyPath
      let encryptedText = encryptText key plainText
      writeFile "encrypted.txt" encryptedText
    ["decrypt", keyPath, encryptedTextPath] -> do
      key <- readKeyFile keyPath
      encryptedText <- readFile encryptedTextPath
      let plainText = decryptAndWriteToFile key encryptedText
      writeFile ("decrypted.txt") plainText
    ["generate-keys", keyPath] -> generateKeyFiles keyPath
    _ -> do
      putStrLn "Usage:"
      putStrLn "encrypt [path to public key file] [path to plain text file]"
      putStrLn
        "decrypt [path to private key file] [path to encrypted text file]"
      putStrLn "generate-keys [path to directory for private and public key]"
