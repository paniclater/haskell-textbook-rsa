# textbook-rsa

Naive, Textbook implementation of RSA in Haskell for educational purposes only. This is not a secure cipher, do not actually use it!

Credit to [Cracking Codes with Python](https://nostarch.com/crackingcodes) for teaching me about this method of implementing textbook RSA encryption and decryption! It's a fantastic book. I strongly recommend reading it and running through all the exercises in Haskell, and maybe learning Python along the way!

Also, if you're really interested in this stuff I strongly recommend a course that Stanford offers for free on Coursera that very thoroughly explores cryptography: [Cryptography I](https://www.coursera.org/learn/crypto)

To use
1. clone project
2. build project
```
stack setup
stack build
```
3. The executable is a cli
```
-- first generate private and public keys
stack exec textbook-rsa-exe generate-keys .
-- then encrypt a piece of text
stack exec textbook-rsa-exe encrypt publicKey.txt yourPlainTextFile.txt
-- then decrypt a piece of text
stack exec textbook-rsa-exe decrypt privateKey.txt encrypted.txt
```

Remember NEVER EVER use this for production encryption.
## Generating Keys


1. Create 2 distinct, very large prime numbers p & q. Multiply them to get a number n.
2. Create a random number e, which is relatively prime to (p - 1) x (q - 1)
3. Calculate the modular inverse of e, and store in d

How do we accomplish this in Haskell?

1. Generate a large random numger
2. Check if it is prime with the efficient isPrime check from Math.NumberTheory.Primes.Testing package
3. Repeat until the check passes

```
generateRandom = do
  let minBound = 2 ^ (keySize - 1)
  let maxBound = 2 ^ (keySize)
  n <- randomRIO (minBound, maxBound)
  return n

generateLargePrime :: IO Integer
generateLargePrime = iterateUntil isPrime generateRandom
```
4. Collect two large primes in a tuple (p, q), and keep generating them until you get two different ones.
5. Get the product of p and q and store it in n
6. Find a number e such that the greatest common divisor of e an p - 1 * q - 1 is one
7. Find the modular inverse of e and p - 1 * q - 1.
8. The modular inverse is a maybe, so unwrap it with a case statement.
9. Now we've got everthing we need for the keys, so write the contents to a file.
```
generateLargePrimeTuple = do
  p <- generateLargePrime
  q <- generateLargePrime
  return (p, q)

generateKeyFiles path = do
  (p, q) <- iterateUntil (\(p, q) -> p /= q) generateLargePrimeTuple
  let n = p * q
  e <- iterateUntil (\e -> (E.gcd e ((p - 1) * (q - 1))) == 1) generateRandom
  let mD = findModInverse e ((p - 1) * (q - 1))
  case mD of
    (Just d) -> do
      let publicKey = (n, e)
      let privateKey = (n, d)
      writeFile (path <> "/publicKey.txt") $
        (show keySize) <> "\n" <> (show $ fst publicKey) <> "\n" <>
        (show $ snd publicKey)
      writeFile (path <> "/privateKey.txt") $
        (show keySize) <> "\n" <> (show $ fst privateKey) <> "\n" <>
        (show $ snd privateKey)
      putStrLn $
        "generated keys at " <> path <> "/publicKey.txt and " <> path <>
        "/privateKey.txt"
    Nothing -> error "Could not find modular inverse."
```

## Converting Text To Blocks
The core idea here is that we take a list of valid characters and a string. We start with 0 and then convert each character into a number by multipliying the index of the character in the valid chars list and multiply it by the number of valid characters raised to the power of the character in the source string.
Example:

valid characters "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

source string "CBA"

C -> 2 * 26 ^ 0

B -> 1 * 26 ^ 1

A -> 0 * 26 ^ 3

Sum them p to gt the block integer 28

We will create a block integer like this for chunks of text in the size of 179, which is the largest safesize for a block given our number of allowed characters.

How do we accomplish this in Haskell?
1. Make a list of valid characters and then zip them up into two maps, one with characters for keys and the other with indices for keys for fast lookups
```
validChars =
  (['A' .. 'Z'] <> ['a' .. 'z'] <> (map toEnum [49 .. 57]) <> ['0'] <> " !?.")

validCharsWithIndex = Map.fromList $ zip validChars [0,1 ..]

indicesWithValidChars = Map.fromList $ zip [0,1 ..] validChars

```
2. Create a newtype wrapper for Block and BlockSize 
```
newtype Block =
  Block Integer
  deriving (Show, Ord, Eq, Generic)

instance Semigroup Block where
  (<>) (Block x) (Block y) = Block (x + y)

instance Monoid Block where
  mempty = Block 0
  mappend = (<>)

newtype BlockSize =
  BlockSize Integer
  deriving (Show, Ord, Eq, Generic)

```
3. Write a function that takes a characters position in the source string and a character and converts it to a block integer 
```
charToBlock p c =
  let index = Map.lookup c validCharsWithIndex
      numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer
  in case index of
       (Just i) -> Block $ i * (numValidChars ^ p)
       _ -> error $ "Unsupported Symbol: " <> [c]
```
4. Write a function that folds over a chunk of characters, converting each to a block and summing the results together
```
chunkToBlock c =
  fst $ foldl (\(b, p) c -> (b <> (charToBlock p c), p + 1)) (Block 0, 0) c
```
5. Map our folding function over each chunk of text in the source string
```
stringToBlocks (BlockSize bS) s =
  map chunkToBlock (chunksOf (fromIntegral bS) s)
```
## Encrypting Blocks
To encrypt a block int b, we just need to take our public key, pull out each number of the tuple into (n, e) and perform b ^ e mod n. This is a pretty intense computation, so we'll lean on the the `powMod` implementation from the Math.NumberTheory.Power.Modular package
```
encryptBlocks key message =
  let (n, e) = key
      blocks = stringToBlocks blockSize message
  in (\(Block b) -> powMod b e n) <$> blocks
```

## Decrypting Blocks
To decrypt a block, we just need to take the block b, grab the tuple (n, d) from our private key and perform b ^ d mod n. Again we'll lean on the powMod function. Also, remember here that `<$>` is just an alias for fmap. 
```
-- these are the same thing:
-- (\(Block b) -> Block (powMod b d n)) <$> encryptedBlocks
-- fmap (\(Block b) -> Block (powMod b d n)) encryptedBlocks

decryptMessage key messageLength encryptedBlocks =
  let (n, d) = key
      blocks = (\(Block b) -> Block (powMod b d n)) <$> encryptedBlocks
  in blocksToString blockSize messageLength blocks
```

## Converting Blocks To Text
Finally we need to convert our decrypted blocks back to text. To do this we need to reverse the process of converting them to blocks starting with the last charachter.

For each block, the current character's index in the map is integer division of the block int with the number of valid characters to the power of the characters index. 
The next block is the result of modular division of the same formula!
1. We can use that formula to write a function that takes a block int and the characters position in the source string and return a tuple of the found character and the next block int.
```
blockToChar b p =
  let numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer
      (index, nextB) = b `divMod` (numValidChars ^ p)
      char = Map.lookup index indicesWithValidChars
  in case char of
       (Just c) -> (c, nextB)
       _ -> error $ "Out of bounds index " <> (show index)
```
2. Then we can recursively solve for each character, starting at the last index of the source string and terminating recursion when the source string index is -1
```
blockToChunk :: Integer -> Block -> String                                                              
blockToChunk bS (Block b) = go b bS                                                                     
  where
    numValidChars = fromIntegral (Map.size validCharsWithIndex) :: Integer                              
    go _ (-1) = ""                                                                                      
    go b bS = 
      let (c, nextB) = blockToChar b bS                                                                 
      in c : (go nextB (bS - 1)) 
```

And that's a naive, textbook rsa in Haskell!
